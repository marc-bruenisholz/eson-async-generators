import eson from 'eson'

    const list_text = `-
 myKey my誰もいないセValue
 .empty list
 anotherKey anotherValue
 anotherKey anotherValue2
 #ignored line
 -key for a sub map
  key value
  a b
  #ignored sub tree
    this is a comment
   over several lines
  .listKey
   1
   #2
   3
 .a list in a list
  .
asdfef
234234`
    const map_text = `myKey my誰もいないセValue
.empty list
anotherKey anotherValue
#ignored line
-key for a sub map
 key value
 a b
 #ignored sub tree
   this is a comment
    wefwefi
  over several lines
 .listKey
  1
  #2
  3
.a list in a list
 .`
console.log(await eson.map(map_text))
console.log(await eson.list(list_text))
const abortController = new AbortController()
const signal = abortController.signal
//abortController.abort()
for await(let element of eson.fromUrl('http://localhost:8000/test.eson', {signal})) {
 console.log(element)
}
