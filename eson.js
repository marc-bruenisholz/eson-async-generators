"use strict";

function printLocation(line, column) {
    return `At ${line}:${column}: `
}

function throwError(line, column, message) {
    throw new Error(printLocation(line, column) + message)
}

async function* UInt8SourceFromUrl(url, signal) {
    const response = await fetch(url, {signal})
    if (!response.ok) { throw response }
    // chunk is supposed to be an Uint8Array
    for await (const chunk of response.body) {
	for (let i = 0; i < chunk.length; i++) {
	    yield chunk[i]
	}
    }
    return null
}

async function* UInt8SourceFromText(text) {
    const encoder = new TextEncoder()
    const byteArray = encoder.encode(text)
    for(let i = 0; i < byteArray.length; i++) {
	yield byteArray[i]
    }
    return null
}



const SEND = Symbol("send")
const LEFT_SHIFT = Symbol("left shift")
const ADD_MAP_TO_LIST = Symbol("add map to list")
const ADD_LIST_TO_LIST = Symbol("add list to list")
const START_KEY_FOR_MAP = Symbol("start key for map")
const END_KEY_FOR_MAP = Symbol("end key for map")
const START_KEY_FOR_LIST = Symbol("start key for list")
const END_KEY_FOR_LIST = Symbol("end key for list")
const START_VALUE_TO_LIST = Symbol("start value to list")
const END_VALUE_TO_LIST = Symbol("end value to list")
const START_KEY_FOR_VALUE = Symbol("start key for value")
const END_KEY_FOR_VALUE = Symbol("end key for value")
const START_VALUE_TO_MAP = Symbol("start value to map")
const END_VALUE_TO_MAP = Symbol("end value to map")
const EMPTY_PARENT = Symbol("empty parent")

const START_COMMENT = Symbol("start comment")
const END_COMMENT = Symbol("end comment")

async function* EsonLexer(options, uint8Source) {
    const SPACE = 32 //' '
    const LIST = 'list'
    const MAP = 'map'
    const EOF = null
    const LINE_FEED = 10 //'\n'
    const NULL = null
    const DOT = 46 //'.'
    const DASH = 45 //'-'
    const BACKSLASH = 92 //'\\'
    const SHARP = 35 //'#'

    let context
    if(options.top === undefined || options.top === 'list') {
	context = LIST
    } else if(options.top === 'map') {
	context = MAP
    } else {
	throw new Error(`EsonParser: Unknown value '${options.top}' for option 'top'. Allowed values: 'map' or 'list'`)
    }

    let stack = [context]
    let lvl = 0
    let maxLvl = 0
    let lastContext = NULL
    let nextContext = NULL
    let cnt = 0

    let c = null
    let line = 1
    let column = 0
    let initialized = false

    let n = async function(){
        c = (await uint8Source.next()).value
        if (c === LINE_FEED) {
            column = 0
            line++
        } else if(c !== EOF) {
            column++
        }
    }

    let readUntilEndOfLine = async function*(){
        while (!(c === LINE_FEED ||  c === EOF)) {
            yield [SEND, line, column, c]
            await n()
        }
    }

    let escape = async function*(characters){
        if (c === BACKSLASH) {
            await n()
            while (c === BACKSLASH) {
                
                yield [SEND, line, column, BACKSLASH]
                await n()
            }
            if (characters.indexOf(c) === -1) {

                yield [SEND, line, column, BACKSLASH]
            }
        }
    }

    let readUntilSpace = async function*(){
        while (!(c === LINE_FEED || c === EOF)) {
            yield [SEND, line, column, c]
            await n()
            if (c === SPACE) {
                await n()
                break
            }
            yield* escape([SPACE])
        }
    }

    
    let leftShift = function*(){
        if (cnt < lvl) {
            yield [LEFT_SHIFT, line, column, lvl - cnt]
            do {
		lvl--
		stack.pop()
		context = stack[stack.length - 1]
            } while (cnt < lvl)
	}
    }

    
    while(true) {
        if (!initialized) {
            await n()
            if (c === SPACE) {
                throwError(line, column, "Didn't expect space")
            }
            initialized = true
        }
        
        if (c === DOT || c === DASH) {
            if (c === DOT) {
                nextContext = LIST
            } else if (c === DASH) {
                nextContext = MAP
            }
            await n()
            lastContext = context
            context = nextContext
            stack.push(context)
            if (lastContext === LIST) {
                if (c === LINE_FEED || c === EOF) {
                    if (nextContext === MAP) {
                        yield [ADD_MAP_TO_LIST, line, column] 
                    } else if (nextContext === LIST) {
                        
                        yield [ADD_LIST_TO_LIST, line, column] 
                    }
                } else {
                    throwError(line, column, "Expected LINE_FEED but got " + String.fromCharCode(c))
                }
            } else if (lastContext === MAP) {
                if (nextContext === MAP) {
                    
                    yield [START_KEY_FOR_MAP, line, column]
                    yield* readUntilEndOfLine()
                    
                    yield [END_KEY_FOR_MAP, line, column]
                } else if (nextContext === LIST) {
                    
                    yield [START_KEY_FOR_LIST, line, column]
                    yield* readUntilEndOfLine()
                    
                    yield [END_KEY_FOR_LIST, line, column]
                }
            }
        } else if (c === SHARP) {
	    yield [START_COMMENT, line, column]
            while (true) {
                await n()
                while (c !== LINE_FEED) {
                    if (c === EOF) {
                        return null
                    }
		    yield [SEND, line, column, c]
                    await n()
                }
                await n()
                cnt = 0
                let result = 0
                while (true) {
                    if (c === SPACE) {
                        cnt++
                        if (cnt > lvl) {
			    yield [SEND, line, column, 10] // \n
                            break
                        } else {
                            await n()
                        }
                    } else {
                        result = 1
                        break
                    }
                }
                if (result === 1) {
                    break
                }
            }
	    yield [END_COMMENT, line, column]
            yield* leftShift()
            continue
        } else {
            if (context === LIST) {
                
                yield [START_VALUE_TO_LIST, line, column]
                yield* escape([SPACE, SHARP, DASH, DOT])
                yield* readUntilEndOfLine()
                
                yield [END_VALUE_TO_LIST, line, column]
            } else if (context === MAP) {
                
                yield [START_KEY_FOR_VALUE, line, column]
                yield* escape([SPACE, SHARP, DASH, DOT])
                yield* readUntilSpace()
                
                yield [END_KEY_FOR_VALUE, line, column]
                
                yield [START_VALUE_TO_MAP, line, column]
                yield* readUntilEndOfLine()
                
                yield [END_VALUE_TO_MAP, line, column]
            }
        }
        if (c === EOF) {
            return null
        } else {
            await n()
            if (nextContext !== NULL) {
                maxLvl = lvl + 1
            } else {
                maxLvl = lvl
            }
            cnt = 0
            while (c === SPACE) {
                cnt++
                if (cnt > maxLvl) {
                    throwError(line, column, "Expected at most " + maxLvl + " spaces")
                } else {
                    await n()
                }
            }
            if (cnt <= lvl) {
                if (nextContext != NULL) {
                    
                    yield [EMPTY_PARENT, line, column]
                    stack.pop()
                    context = stack[stack.length - 1]
                }
                yield* leftShift()
            } else {
                lvl++
            }
            nextContext = NULL
        }
    }
}

const ADD_VALUE_TO_LIST = Symbol("add value to list")
const ADD_VALUE_TO_MAP = Symbol("add value to map")
const ADD_LIST_TO_MAP = Symbol("add list to map")
const ADD_MAP_TO_MAP = Symbol("add map to map")
const COMMENT = Symbol("comment")

async function *BufferedEsonLexer(options, esonLexer) {
    const bufferSize = options.bufferSize || 4096
    let buffer1 = new Uint8Array(bufferSize)
    let buffer2 = new Uint8Array(bufferSize)
    let activeBuffer = buffer1
    let activeBufferI = 0
    let keyBuffer
    let keyBufferI
    let token
    let startLine1
    let startColumn1
    let startLine2
    let startColumn2

    while(token = (await esonLexer.next()).value) {
        let t = token[0]
	let line = token[1]
	let column = token[2]
	// TODO proper state machine
        if (t === LEFT_SHIFT ||
            t === ADD_LIST_TO_LIST ||
            t === ADD_MAP_TO_LIST ||
            t === EMPTY_PARENT
           ) {
            yield token
        } else if (t === START_VALUE_TO_LIST ||
                   t === START_KEY_FOR_VALUE ||
                   t === START_KEY_FOR_LIST ||
                   t === START_KEY_FOR_MAP ||
		   t === START_COMMENT) {
            activeBufferI = 0
	    startLine1 = line
	    startColumn1 = column
	} else if (t === START_VALUE_TO_MAP) {
	    activeBufferI = 0
	    startLine2 = line
	    startColumn2 = column
	} else if (t === END_COMMENT) {
	    yield [COMMENT, startLine1, startColumn1, activeBuffer, activeBufferI]
	} else if (t === END_VALUE_TO_LIST) {
            yield [ADD_VALUE_TO_LIST, startLine1, startColumn1, activeBuffer, activeBufferI]
        } else if (t === END_KEY_FOR_VALUE) {
	    keyBufferI = activeBufferI
	    if (activeBuffer === buffer1) {
		keyBuffer = buffer1
		activeBuffer = buffer2
	    } else {
		keyBuffer = buffer2
		activeBuffer = buffer1
	    }
        } else if (t === END_VALUE_TO_MAP) {
            yield [ADD_VALUE_TO_MAP, startLine1, startColumn1, startLine2, startColumn2, keyBuffer, keyBufferI, activeBuffer, activeBufferI]
        } else if (t === END_KEY_FOR_LIST) {
            yield [ADD_LIST_TO_MAP, startLine1, startColumn1, activeBuffer, activeBufferI]
        } else if (t === END_KEY_FOR_MAP) {
            yield [ADD_MAP_TO_MAP, startLine1, startColumn1, activeBuffer, activeBufferI]
        } else if (t === SEND) {
	    if(activeBufferI >= bufferSize) {
		throwError(line, column, 'String exceeds buffer size')
	    }
	    activeBuffer[activeBufferI] = token[3]
            activeBufferI++
        } else {
            throwError(line, column, `Implementation error. Unexpected token ${t}`)
        }
    }
    return null
}

async function *EsonParser(options, bufferedEsonLexer) {
    let encoding = options.encoding || 'utf8'
    let throwOnDuplicateKey = options.throwOnDuplicateKey || false
    let _top
    if(options.top === undefined) {
	options.top = 'list'
    }
    if(options.top === 'list') {
	_top = []
    } else if(options.top === 'map') {
	_top = {}
    } else {
	throw new Error(`EsonParser: Unknown value '${options.top}' for option 'top'. Allowed values: 'map' or 'list'`)
    }
    let stack = [_top]
    let token
    let key
    let value
    const decoder = new TextDecoder(encoding);

    function push(parent){
        stack.push(parent)
        _top = parent
    }
    function* pop(){
        if (options.top == 'list' && stack.length === 2) {
            yield stack[1]
        }
        stack.pop()
        _top = stack[stack.length - 1]
    }
    function addParentToList(parent){
        _top.push(parent)
        push(parent)
    }
    function addParentToMap(key,parent){
        _top[key] = parent
        push(parent)
    }
    
    while(token = (await bufferedEsonLexer.next()).value) {
        let t = token[0]
	let line = token[1]
	let column = token[2]
	// TODO proper state machine
        if(t === LEFT_SHIFT) {
            let n = token[3]
            for (; n > 0; n--)
                yield* pop()
        } else if(t === ADD_VALUE_TO_LIST) {
	    value = decoder.decode(token[3].subarray(0, token[4]))
            if(stack.length === 1) {
                yield value
            }
            _top.push(value)
        } else if(t === ADD_VALUE_TO_MAP) {
	    // Debug info for the map value:
	    //let line2 = token[3]
	    //let column2 = token[4]
	    key = decoder.decode(token[5].subarray(0,token[6]))
	    value = decoder.decode(token[7].subarray(0,token[8]))
	    if(throwOnDuplicateKey && (key in _top)) {
		throwError(line, column, `The key '${key}' already exists on the object`)
	    }
            _top[key] = value
        } else if(t === ADD_LIST_TO_LIST) {
            addParentToList([])
        } else if(t === ADD_LIST_TO_MAP) {
	    key = decoder.decode(token[3].subarray(0,token[4]))
            addParentToMap(key, [])
        } else if(t === ADD_MAP_TO_LIST) {
            addParentToList({})
        } else if(t === ADD_MAP_TO_MAP) {
	    key = decoder.decode(token[3].subarray(0,token[4]))
            addParentToMap(key, {})
        } else if(t === EMPTY_PARENT) {
            yield* pop()
	} else if(t === COMMENT) {
	    // This is how comments may be used:
	    //let comment = decoder.decode(token[3].subarray(0,token[4]))
        } else {
            throwError(line, column, `Unexpected token ${t}. Likely an implementation error.`)
        }
    }

    if (options.top === 'list') {
	for (; stack.length > 1;)
	    yield* pop()
    } else if (options.top === 'map') {
	yield stack[0]
    } else {
	throw new Error('Implementation error')
    }
    
    
    return null
}

function defaultOptions(options) {
    return {
	throwOnDuplicateKey : options?.throwOnDuplicateKey || false,
	bufferSize : options?.bufferSize || 4096
    }
}
async function list(text, options) {
    const {throwOnDuplicateKey, bufferSize} = defaultOptions(options)
    const list = []
    for await(let element of EsonParser({encoding: 'utf8', top: 'list', throwOnDuplicateKey}, BufferedEsonLexer({bufferSize}, EsonLexer({top: 'list'}, UInt8SourceFromText(text))))) {
	list.push(element)
    }
    return list
}

async function map(text, options) {
    const {throwOnDuplicateKey, bufferSize} = defaultOptions(options)
    return (await EsonParser({encoding: 'utf8', top: 'map', throwOnDuplicateKey}, BufferedEsonLexer({bufferSize}, EsonLexer({top:'map'}, UInt8SourceFromText(text)))).next()).value
}

function fromUrl(url, options) {
    const {throwOnDuplicateKey, bufferSize} = defaultOptions(options)
    return EsonParser({encoding: 'utf8', top: 'list', throwOnDuplicateKey}, BufferedEsonLexer({bufferSize}, EsonLexer({top: 'list'}, UInt8SourceFromUrl(url, options?.signal))))
}

export default { list, map, fromUrl }
