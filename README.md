# Eson

Eson is a data format based on indenting and line breaks. A demo is available here: https://marc-bruenisholz.gitlab.io/eson/.

It supports comments and streaming of objects.

# How to run

This project contains javascript code in eson.js which is isomorphic:

"Isomorphic JavaScript, also known as Universal JavaScript, describes JavaScript applications which run both on the client and the server." - Wikipedia

This means, it runs in the browser (client), node or deno (server).

## Browser

Run a local server. E.g. with python:

    python -m http.server 8000

and open the website http://localhost:8000/index.html. The page defines a import map which maps the import name "eson" to http://localhost:8000/eson.mjs. The javascript module is loaded and some example eson texts are parsed and printed on the console. The last example loads the contents from http://localhost:8000/test.eson and parses it as a stream.

## Node

Create a soft link at `node_modules/eson/eson.js` that points to `eson.js` if it does not exist already. This way, node can map the import name "eson" to that file.

    ln -s ../../eson.js node_modules/eson/eson.js
 
There is a `package.json` in `node_modules/eson` that defines the type of the package as a "module". To run `main.mjs` type:

    node main.mjs

The file `package.json` in the working directory sets the package type to "module" so that the modules syntax is supported.
 
## Deno

Deno also uses an import map which is located at deno.js. It maps "eson" to the file `eson.js`. Network access must be explicitly allowed with a CLI option:

    deno run --allow-net main.mjs

# Streaming

This eson parser uses chained, asynchronous generator functions to allow streaming of data. This means that chunks of data can be loaded, e.g. over the network, and processed as soon as they arrive.

